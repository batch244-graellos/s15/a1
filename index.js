// console.log("Hello World");

	let firstName = "First Name: Carl";
	console.log(firstName);

	let lastName = "Last Name: Graellos";
	console.log(lastName);

	let myAge = "30";
	console.log("Age: " + myAge);

	let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
	console.log("Hobbies: " + hobbies);

	let contact = {
			houseNumber: 32,
			street: "Washington",
			city: "Lincoln",
			state: "Nebraska"
		}
	console.log("Work Address: ");
	console.log(contact);

	let myFullName = "Carl Graellos";
	console.log("My full name is: " + myFullName);

	console.log("My current age is: " + myAge)

	let myFriends = ["Caiden","Saffiyah", "Jean", "Ysabella", "Sairah"];
	console.log("My Friends are: ");
	console.log(myFriends);

	let Profile = {
			username: "carl_graellos",
			fullName: "Carl Graellos",
			age: 30,
			isActive: false,
		}
	console.log("My Full Profile: ");
	console.log(Profile);

	let myBestFriend = "Mary Fe";
	console.log("My Best Friend is: " + myBestFriend);

	let found = "Artic Ocean";
	console.log("I was found frozen in: " + found);



	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce", "Thor", "Natasha","Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";// This will result to an error: Assigment to a constant variable
	console.log("I was found frozen in: " + lastLocation);

